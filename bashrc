
# If not running interactively, don't do anything
[ -z "$PS1" ] && return


if [ -f /etc/bashrc ]; then
    . /etc/bashrc   # --> Read /etc/bashrc, if present.
fi


user_color=true
export EDITOR=vim
export HISTSIZE=10000

# script may be run form another dir
source $HOME/config-helpers/bash/bash_alias
source $HOME/config-helpers/bash/bash_prompt

if [ -f /usr/share/bash-completion/completions/git ]; then
  source /usr/share/bash-completion/completions/git
fi

alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias open='xdg-open'

# Save SSH Password in KWallet
if which ksshaskpass >/dev/null 2>&1; then
  eval $(ssh-agent) >/dev/null
  # If you key is called id_rsa then this will work automatically
  # If you have a different name then export REMEMBER_SSH_KEYS
  # env var with the path of the key you want e.g.
  # export REMEMBER_SSH_KEYS="$HOME/.ssh/jroddev_titan_id_rsa"
  SSH_ASKPASS=ksshaskpass ssh-add ${REMEMBER_SSH_KEYS} < /dev/null
fi
