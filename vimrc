" bring in a smaple vimrc as a starting point
source $HOME/config-helpers/vim/example-vimrc

set number              " show line numbers
set mouse=a             " enable mouse support (might not work well on Mac OS X)
set encoding=utf-8
set wildmenu            " visual autocomplete for command menu
set paste
set cursorline		" highligh current line

" Custom colorcolumn
set colorcolumn=80
execute "set colorcolumn=" . join(range(81,335), ',')
highlight ColorColumn ctermbg=black

" Indentation spaces
set shiftwidth=2
set softtabstop=2
set expandtab

" Turn backup off, most is in version control anyway
set nobackup
set nowb
set noswapfile

" :W sudo saves the file
" (useful for handling the permission-denied error)
command W w !sudo tee % > /dev/null


" Source .vimrc file in present working directory (proj specific config)
set exrc
" restrict usage of commands from local .vimrc file
set secure


" Highlights trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Trims trailing whitespace on save
function! TrimWhiteSpace()
  %s/\s*$//
    ''
endfunction
autocmd FileWritePre * call TrimWhiteSpace()
autocmd FileAppendPre * call TrimWhiteSpace()
autocmd FilterWritePre * call TrimWhiteSpace()
autocmd BufWritePre * call TrimWhiteSpace()

