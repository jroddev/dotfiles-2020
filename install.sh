#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
DIR="$(cd "$(dirname "$0")" && pwd)"
echo "Installing dotfiles configuration"

# name, config file, symlink
function installConfig () {
	echo ""
	echo -e "${NC}Installing $1"
	if [ -f $3 ]; then
		if [ ! -L $3 ]; then
			echo -e "${RED}File $3 already exists! Please remove before installing"
		else
			echo -e "${YELLOW}Symlink $3 already exists. Skipping"
		fi
	else

		echo -e "${GREEN}Symlinking $3 to $2"
		ln -sf $2 $3

	fi
}

if [ ! -L $HOME/config-helpers ]; then
	echo -e "${GREEN}Creating symlink to config helpers at $HOME/config-helpers"
	ln -sf $DIR/config-helpers $HOME/config-helpers
fi

installConfig bashrc $DIR/bashrc $HOME/.bashrc
installConfig vimrc $DIR/vimrc $HOME/.vimrc
installConfig tmux $DIR/tmux.conf $HOME/.tmux.conf

mkdir -p $HOME/.config/alacritty
installConfig alacritty $DIR/alacritty.yml $HOME/.config/alacritty/alacritty.yml


